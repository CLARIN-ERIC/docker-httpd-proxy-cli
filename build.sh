#!/bin/bash

set -e

#
# Set default values for parameters
#
MODE="gitlab"
BUILD=0
TEST=0
RELEASE=0
VERBOSE=0

#
# Process script arguments
#
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -b|--build)
        BUILD=1
        ;;
    -l|--local)
        MODE="local"
        ;;
    -h|--help)
        MODE="help"
        ;;
    -r|--release)
        RELEASE=1
        ;;
    -t|--test)
        TEST=1
        ;;
    -v|--verbose)
        VERBOSE=1
        ;;
    *)
        echo "Unkown option: $key"
        MODE="help"
        ;;
esac
shift # past argument or value
done

# Print parameters if running in verbose mode
if [ ${VERBOSE} -eq 1 ]; then
    echo "build=${BUILD}"
    set -x
fi

#
# Execute based on mode argument
#
if [ ${MODE} == "help" ]; then
    echo ""
    echo "build.sh [-lt]"
    echo ""
    echo "  -b, --build      Build docker image"
    echo "  -r, --release    Push docker image to registry"
    echo "  -t, --test       Execute tests"
    exho ""
    echo "  -l, --local      Run workflow locally in a local docker container"
    echo "  -v, --verbose    Run in verbose mode"
    echo ""
    echo "  -h, --help       Show help"
    echo ""
    exit 0
elif [ "${MODE}" == "gitlab" ]; then

    if [ -n "$CI_SERVER" ]; then
        TAG="${CI_BUILD_TAG:-$CI_BUILD_REF}"
        IMAGE_QUALIFIED_NAME="${CI_REGISTRY_IMAGE}:${TAG}"
        IMAGE_FILE_NAME="${CI_REGISTRY_IMAGE##*/}:${TAG}"
    else
        # WARNING: The current working dir must equal the project root dir.
        apk --quiet update --update-cache
        apk --quiet add 'git==2.8.3-r0'
        PROJECT_NAME="$(basename "$(pwd)")"
        TAG="$(git describe --always)"
        IMAGE_QUALIFIED_NAME="$PROJECT_NAME:${TAG:-latest}"
        IMAGE_FILE_NAME="${IMAGE_QUALIFIED_NAME}"
    fi
    IMAGE_FILE_PATH="$(readlink -fn './output/')/$IMAGE_FILE_NAME.tar.gz"

    export IMAGE_QUALIFIED_NAME
    export IMAGE_FILE_PATH

    #Build
    if [ "${BUILD}" -eq 1 ]; then
        echo "**** Building image ****"
        cd -- 'image/'
        docker build --tag="$IMAGE_QUALIFIED_NAME" .
        docker save --output="$IMAGE_FILE_PATH" "$IMAGE_QUALIFIED_NAME"
    fi

    #Test
    if [ "${TEST}" -eq 1 ]; then
        echo "**** Testing image ****"
        cd -- 'run/'
        apk --quiet update --update-cache
        apk --quiet add 'py-pip==8.1.2-r0'
        pip --quiet --disable-pip-version-check install 'docker-compose==1.8.0'
        docker load --input="$IMAGE_FILE_PATH"
        docker-compose -f 'docker-compose.yml' -f 'deployment.yml' up 'proxy-preparer'
        number_of_failed_containers="$(docker-compose ps -q | xargs docker inspect \
            -f '{{ .State.ExitCode }}' | grep -c 0 -v | tr -d ' ')"
        exit "$number_of_failed_containers"
    fi

    #Release
    if [ "${RELEASE}" -eq 1 ]; then
        echo "**** Releasing image ****"
        docker login -u 'gitlab-ci-token' -p "${CI_BUILD_TOKEN}" 'registry.gitlab.com'
        docker load --input="${IMAGE_FILE_PATH}"
        docker push "${IMAGE_QUALIFIED_NAME}"
        docker logout 'registry.gitlab.com'
    fi

elif [ "${MODE}" == "local" ]; then

    FLAGS=""
    if [ ${VERBOSE} -eq 1 ]; then
        FLAGS="-x"
    fi

    CMD=""
    if [ ${BUILD} -eq 1 ] && [ ${TEST} -eq 1 ]; then
        CMD="sh ${FLAGS} ./build.sh --build && sh ${FLAGS} ./build.sh --test"
    elif [ ${BUILD} -eq 1 ]; then
        CMD="sh ${FLAGS} ./build.sh --build"
    elif [ ${TEST} -eq 1 ]; then
        CMD="sh ${FLAGS} build.sh --test"
    fi

    docker run \
        --volume='/var/run/docker.sock:/var/run/docker.sock' \
        --rm \
        --volume="$PWD":"$PWD" \
        --workdir="$PWD" \
        -it \
        docker:1.11.2 \
        sh -c "${CMD}"
else
    exit 1
fi
